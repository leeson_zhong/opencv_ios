//
//  ViewController.m
//  TestOpencv
//
//  Created by leeson zhong on 2020/7/21.
//  Copyright © 2020 octant. All rights reserved.
//
#import <opencv2/opencv.hpp>
#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.messageLabel.text = [NSString stringWithFormat:@"OPENCV VERSION :%s",CV_VERSION];
}


@end
