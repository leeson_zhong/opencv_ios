## 在 iOS 上集成 OpenCV 库的方法

1. 使用Cocopods进行管理依赖 pod ''OpenCV''
2. 在官网[OpenCV-iOS framework](https://opencv.org/releases.html)直接下载编译好的库
3. 从 [GitHub](https://github.com/opencv/opencv) 拉下源码，[编译](https://docs.opencv.org/master/d5/da3/tutorial_ios_install.html)成framework，导入工程中

## 构建[opencv](https://github.com/opencv/opencv) + [opencv_contrib](https://github.com/opencv/opencv_contrib)的framework

- 标准模块：[opencv](https://github.com/opencv/opencv)

- 额外的模块：[opencv_contrib](https://github.com/opencv/opencv_contrib)

- 获取OpenCV标准模块的源代码。将其存储在任何文件夹中，我们将其称为<opencv_source_path>

- 获取OpenCV的额外模块的源代码。将其存储在任何文件夹中，我们将其称为<opencv_contrib_source_path>

- 尝试构建所有模块并将构建存储在任何文件夹中，我们将其称为<opencv_contrib_build_path>

- 运行脚本进行编译

  ```xml
  $ ./<opencv_source_path>/platforms/ios/build_framework.py <opencv_contrib_build_path> --contrib <opencv_contrib_source_path>
  ```

- 如果任何模块未能构建，通过删除模块或修复其源代码来解决问题。如果我们不需要有问题的模块，我们可以简单地在/modules中删除它的源子文件夹，然后重新运行build_framework.py。例如，为了避免构建显著性模块，我们可以删除/modules/saliency

- github下载代码太慢了，可以通过码云转存github代码再下载

- 提示Permission denied的，使用`$ chmod 777 ./opencv/platforms/ios/build_framework.py`添加权限

- 编译时提示check_call(cmd, cwd = cwd)错误，安装[cmake](https://cmake.org/install/)，可以下载源代码

  ```
  ./bootstrap
    make
    make install
  ```

  或使用

  `brew install cmake`

- 编译opencv标准库出现了问题，网上找不到对应的问题和解决方法。重新从官方的github下载最新代码，编译没有出现同样问题了。（原来的源代码我是从第三方网址下载的）

- 编译opencv_contrib时xfeatures2d模块提示缺少boostdesc_bgm.i文件，网上下载[缺少的文件](https://github.com/AsaBoring/boostdesc_bgm_files_build_opencv_contrib_needed)放到xfeatures2d/src文件夹中

- 编译成功后，在编译输出的文件夹中可以看到build、opencv2.framework、samples三个文件夹或文件，build是编译时产生的临时文件，opencv2.framework是ios要用的opencv库，sameples是opencv的ios项目例程。

- opencv gitee地址https://gitee.com/leesonzhong/opencv

- opencv contrib gitee地址https://gitee.com/leesonzhong/opencv_contrib

- xfeatures2d缺少的文件 gitee地址https://gitee.com/leesonzhong/boostdesc_bgm_files_build_opencv_contrib_needed

## 配置OpenCV工程

- 导入opencv库。拷贝opencv2.framework到工程目录下，在**general**->**Frameworks,Libraries,and Embedded Contect**添加opencv2.framework依赖，**Embel**选择**Do Not Embed**
- 使用时m文件改成mm文件
- 导入的opencv头文件要放在最上面，比如`#import <opencv2/opencv.hpp>`要放在`#import "ViewController.h"`前面

